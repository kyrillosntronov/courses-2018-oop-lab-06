/**
 * 
 */
package it.unibo.oop.lab.collections2;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * Instructions
 * 
 * This will be an implementation of
 * {@link it.unibo.oop.lab.collections2.SocialNetworkUser}:
 * 
 * 1) complete the definition of the methods by following the suggestions
 * included in the comments below.
 * 
 * @param <U>
 *            Specific user type
 */
public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {

	//Social HashMap, User is key, Group is value
	Map<U, String> socialMap = new HashMap<>();

    public SocialNetworkUserImpl(final String name, final String surname, final String user, final int userAge) {
        super(name, surname, user, userAge);
    }
    
    public SocialNetworkUserImpl(final String name, final String surname, final String user) {
    	super(name, surname, user, -1);
    }

    @Override
    public boolean addFollowedUser(final String circle, final U user) {
       if(socialMap.containsKey(user)){
    	   return false;
       }
       socialMap.put(user, circle);
       return true;
    }

    @Override
    public Collection<U> getFollowedUsersInGroup(final String groupName) {
    	Collection<U> collection = new LinkedList<>();
    	for(U user : socialMap.keySet()) {
    		if(socialMap.get(user).equals(groupName)) {
    			collection.add(user);
    		}
    	}
		return collection;
    }

    @Override
    public List<U> getFollowedUsers() {
        List<U> collection = new LinkedList<>();
        for(U user : socialMap.keySet()) {
        	collection.add(user);
        }
		return collection;
    }

}
